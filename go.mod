module bitbucket.org/lygo/lygo_file_watcher

go 1.14

require (
	bitbucket.org/lygo/lygo_commons v0.1.119
	bitbucket.org/lygo/lygo_events v0.1.0 // indirect
	github.com/google/uuid v1.3.0 // indirect
)
